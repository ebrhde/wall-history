<?php

namespace app\controllers;

use app\models\Message;
use app\models\MessageForm;
use app\interfaces\RequestHandlerInterface;
use app\models\Reaction;
use app\services\FloodLimiter;
use app\services\MessageRateHandler;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    private RequestHandlerInterface $handler;

    public function __construct($id, $module, RequestHandlerInterface $handler, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Отображает домашнюю страницу с опубликованными сообщениями и формой.
     *
     * @return string|Response
     */
    public function actionIndex()
    {
        $userIp = Yii::$app->request->userIP;

        $model = new MessageForm();

        if(Yii::$app->request->isPost) {
            if($time = FloodLimiter::checkExceeding($userIp, 'app\models\Message')) {
                Yii::$app->session->setFlash('error', 'Вы сможете отпроавить новое сообщение в ' . $time . '. Пожалуйста, подождите.');
            } else {
                $sent = $this->handler->run(Yii::$app->request);

                if(!$sent) {
                    Yii::$app->session->setFlash('error', 'Сообщение не отправлено. Пожалуйста, проверьте правильность заполнения и формы и попробуйте ещ раз.');
                } else {
                    return $this->redirect(['/']);
                }
            }
        }

        $messageProvider = new ActiveDataProvider([
            'query' => Message::find()->orderBy(['created_at' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $reactions = Reaction::find()->all();

        return $this->render('index', [
            'model' => $model,
            'messageProvider' => $messageProvider,
            'reactions' => $reactions,
            'userIp' => $userIp
        ]);
    }

    /**
     * Отображает страницу "Правила".
     */
    public function actionRules(): string
    {
        return $this->render('rules');
    }

    /**
     * Принимает и обрабатывает запрос добавления реакции к сообщению
     * @return Response|void
     */
    public function actionRateMessage() {
        $this->handler = Yii::createObject(MessageRateHandler::class);

        if($this->handler->run(Yii::$app->request)) {
            return $this->redirect(['/']);
        }
    }
}
