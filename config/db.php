<?php

$localConfig = require(__DIR__ . '/params-local.php');

return [
    'class' => 'yii\db\Connection',
    'dsn' => $localConfig['db']['dsn'],
    'username' => $localConfig['db']['username'],
    'password' => $localConfig['db']['password'],
    'charset' => $localConfig['db']['charset'],
];
