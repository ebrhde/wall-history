<?php

namespace app\interfaces;

use yii\base\Model;
use yii\web\Request;

interface RequestHandlerInterface
{
    public function run(Request $request): bool;
}