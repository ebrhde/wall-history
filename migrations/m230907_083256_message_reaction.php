<?php

use yii\db\Migration;

/**
 * Class m230907_083256_message_reaction
 */
class m230907_083256_message_reaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%message_reaction}}', [
            'message_id' => $this->integer(11),
            'reaction_id' => $this->integer(11),
            'user_ip' => $this->string(255)
        ], $tableOptions);

        $this->addPrimaryKey('PK_message_reaction', '{{%message_reaction}}', ['reaction_id', 'message_id', 'User_ip']);

        $this->addForeignKey(
            'FK_mr_message',
            '{{%message_reaction}}',
            'message_id',
            '{{%message}}',
            'id',
        );

        $this->addForeignKey(
            'FK_mr_reaction',
            '{{%message_reaction}}',
            'reaction_id',
            '{{%reaction}}',
            'id',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_mr_reaction', '{{%message_reaction}}');
        $this->dropForeignKey('FK_mr_message', '{{%message_reaction}}');
        $this->dropPrimaryKey('PK_message_reaction', '{{%message_reaction}}');

        $this->dropTable('{{%message_reaction}}');
    }
}
