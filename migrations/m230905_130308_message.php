<?php

use yii\db\Migration;

/**
 * Class m230905_130308_message
 */
class m230905_130308_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'author_name' => $this->string(255),
            'author_ip' => $this->string(255),
            'text' => $this->string(1000)
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%message}}');
    }
}
