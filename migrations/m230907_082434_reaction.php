<?php

use yii\db\Migration;

/**
 * Class m230907_082434_reaction
 */
class m230907_082434_reaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%reaction}}', [
            'id' => $this->primaryKey(),
            'emoji_unicode' => $this->string(10)
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%reaction}}');
    }
}
