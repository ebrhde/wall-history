<h1>Wall-History App</h1>

<h2>Описание</h2>

Тестовое задание, представляющее стену с сообщениями. Приложение написано на PHP версии 7.4 с помощью фреймворка Yii2. Реализован функционал добавления новых сообщений и реакций к ним. Так же предусмотрено ограничение отправки сообщений (не больше 1 каждые 3 минуты с одного Ip-адреса).
