<?php

namespace app\models;
use yii\base\Model;

class MessageForm extends Model
{
    public $author;
    public $text;
    public $captcha;

    public function rules()
    {
        return [
            [['author', 'text'], 'required'],
            [['author'], 'string', 'min' => 2, 'max' => 15],
            [['text'], 'string', 'min' => 30, 'max' => 1000],
            ['captcha', 'captcha', 'captchaAction' => 'site/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'author' => 'Автор',
            'text' => 'Сообщение',
            'captcha' => 'CAPTCHA'
        ];
    }

}