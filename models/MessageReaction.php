<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_reaction".
 *
 * @property int $message_id
 * @property int $reaction_id
 * @property string $user_ip
 */
class MessageReaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message_reaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message_id', 'reaction_id', 'user_ip'], 'required'],
            [['message_id', 'reaction_id'], 'integer'],
            [['user_ip'], 'string', 'max' => 255],
            [['message_id', 'reaction_id', 'user_ip'], 'unique', 'targetAttribute' => ['message_id', 'reaction_id', 'user_ip']],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::class, 'targetAttribute' => ['message_id' => 'id']],
            [['reaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reaction::class, 'targetAttribute' => ['reaction_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'message_id' => 'Message ID',
            'reaction_id' => 'Reaction ID',
            'user_ip' => 'User Ip',
        ];
    }

    public function getCount()
    {
        return static::find()->where(['message_id' => $this->message_id, 'reaction_id' => $this->reaction_id])->count();
    }

    public function getReaction()
    {
        return $this->hasOne(Reaction::class, ['id' => 'reaction_id']);
    }
}
