<?php

namespace app\models;

use IntlChar;
use Yii;

/**
 * This is the model class for table "reaction".
 *
 * @property int $id
 * @property string|null $emoji_unicode
 */
class Reaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emoji_unicode'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emoji' => 'Emoji',
        ];
    }

    public function getEmojiCharacter() {
        return IntlChar::chr(hexdec(substr($this->emoji_unicode, 2)));
    }
}
