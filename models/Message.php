<?php

namespace app\models;

use Yii;
use yii\db\Expression;

/**
 *
 * @property int $id
 * @property int $created_at
 * @property string $author_name
 * @property string $author_ip
 * @property string $text
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => function () {
                    return time(); // Use time() to get the current Unix timestamp
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['author_name', 'text'], 'required'],
            [['created_at'], 'integer'],
            [['author_ip'], 'string', 'max' => 255],
            [['author_name'], 'string', 'min' => 2, 'max' => 15],
            [['text'], 'string', 'min' => 30, 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'author_name' => 'Автор',
            'author_ip' => 'IP-адрес автора',
            'text' => 'Сообщение',
        ];
    }

    /**
     * Скрывает два последних сегмента ip-адреса формата IPv4
     */
    public function getHiddenIp(): string
    {
        $explodedIp = explode('.', $this->author_ip);

        if(count($explodedIp) === 4) {
            $explodedIp[2] = str_repeat('*', strlen($explodedIp[2]));
            $explodedIp[3] = str_repeat('*', strlen($explodedIp[3]));
        }

        return implode('.', $explodedIp);
    }

    /**
     * Формирует строку вида "n ... назад" из даты публикации
     */
    public function getRelativeTime(): string
    {
        $currentTimestamp = time();
        $timestampDiff = $currentTimestamp - $this->created_at;

        if ($timestampDiff < 60) {
            return $timestampDiff . ' секунд' . ($timestampDiff > 1 ? '' : 'у') . ' назад';
        } elseif ($timestampDiff < 3600) {
            $minutes = floor($timestampDiff / 60);
            return $minutes . ' минут' . ($minutes > 1 ? '' : 'у') . ' назад';
        } elseif ($timestampDiff < 86400) {
            $hours = floor($timestampDiff / 3600);
            return $hours . ' час' . ($hours > 1 ? 'ов' : '') . ' назад';
        } else {
            $days = floor($timestampDiff / 86400);
            return $days . ($days > 1 ? ' дней' : ' день') . ' назад';
        }
    }

    public function getReactions()
    {
        return $this->hasMany(MessageReaction::class, ['message_id' => 'id'])
            ->viaTable('message_reaction', ['id' => 'message_id']);
    }

    public function getMessageReactions()
    {
        return $this->hasMany(MessageReaction::class, ['message_id' => 'id'])->groupBy(['reaction_id']);
    }

    public function hasReaction($reactionId)
    {
        foreach ($this->messageReactions as $messageReaction) {
            if ($messageReaction->reaction_id == $reactionId) {
                return true;
            }
        }
        return false;
    }
}
