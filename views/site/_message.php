<?php
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;

$purifierConfig = ['HTML.Allowed' => 'b,i,s'];
?>

<div class="card card-default">
    <div class="card-body">
        <h5 class="card-title"><?= HtmlPurifier::process($model->author_name, $purifierConfig); ?></h5>
        <p><?= HtmlPurifier::process($model->text, $purifierConfig); ?></p>
        <p>
            <small class="text-muted">
                <?= $model->getRelativeTime(); ?> |
                <?= $model->getHiddenIp(); ?>
            </small>
        </p>
        <p>
            <div class="btn-group" role="group">
                <?php foreach ($model->messageReactions as $messageReaction): ?>
                    <?php $reaction = $messageReaction->reaction; ?>
                    <?=
                    Html::a($reaction->emojiCharacter . '<span class="badge bg-secondary">' . $messageReaction->count . '</span>',
                            ['site/rate-message', 'reaction' => $reaction->id, 'message' => $model->id],
                            ['class' => $messageReaction->user_ip === $userIp ? 'btn btn-outline-success active' : 'btn btn-outline-success'])
                    ?>
                <?php endforeach; ?>
                <?php foreach ($reactions as $reaction): ?>
                    <?php if (!$model->hasReaction($reaction->id)): ?>
                        <?=
                        Html::a($reaction->emojiCharacter,
                            ['site/rate-message', 'reaction' => $reaction->id, 'message' => $model->id],
                            ['class' => 'btn btn-outline-success'])
                        ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </p>
    </div>
</div>
