<?php

/** @var yii\web\View $this */

use yii\bootstrap5\Alert;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="body-content mt-5">
        <h1>Сообщения</h1>
        <div class="row">
            <div class="col-md-8">
                <?= ListView::widget([
                    'dataProvider' => $messageProvider,
                    'itemView' => '_message',
                    'layout' => "{items}\n{pager}",
                    'viewParams' => [
                        'reactions' => $reactions,
                        'userIp' => $userIp
                    ],
                ]); ?>
            </div>
            <div class="col-md-4">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'author')->textInput(['placeholder' => 'Elon_Musk_777']); ?>
                <?= $form->field($model, 'text')->textarea(['placeholder' => 'Текст сообщения. Допускаются некоторые теги для оформления текста (<b>, <i>, <s>)']); ?>
                <?= $form->field($model, 'captcha')->widget(Captcha::class, [
                    'captchaAction' => 'site/captcha',
                    'template' => '<div class="row align-items-center"><div class="col">Код с картинки</div><div class="col">{image}</div></div><div>{input}</div>',
                ]); ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <?php if(Yii::$app->session->hasFlash('error')) :?>
                <?= Alert::widget([
                    'options' => ['class' => 'alert-danger'],
                    'body' => Yii::$app->session->getFlash('error'),
                ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<style>
    .field-messageform-captcha .control-label {
        display: none;
    }
</style>
