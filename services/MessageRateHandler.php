<?php

namespace app\services;

use app\interfaces\RequestHandlerInterface;
use app\models\MessageReaction;
use yii\web\Request;

class MessageRateHandler implements RequestHandlerInterface
{
    /**
     * Обрабатывает поступившую реакцию на сообщение: удаляет, заменяет или добавляет
     */
    public function run(Request $request): bool
    {
        $userIp = $request->userIP;
        $reactionId = $request->get('reaction');
        $messageId = $request->get('message');

        $messageReaction = MessageReaction::find()
            ->andWhere(['user_ip' => $userIp, 'message_id' => $messageId, 'reaction_id' => $reactionId])
            ->one();

        if($messageReaction) {
            return $messageReaction->delete();
        }

        $messageReaction = MessageReaction::find()
            ->andWhere(['user_ip' => $userIp, 'message_id' => $messageId])
            ->one();

        if($messageReaction) {
            $messageReaction->reaction_id = $reactionId;
            return $messageReaction->save();
        }

        $messageReaction = new MessageReaction();
        $messageReaction->message_id = $messageId;
        $messageReaction->reaction_id = $reactionId;
        $messageReaction->user_ip = $userIp;

        return $messageReaction->save();
    }
}