<?php

namespace app\services;

class FloodLimiter
{
    /**
     * Проверяет, было ли добавлено от пользователя сообщение за предыдущие три минуты. Если да, возвращает временную метку,
     * указывающую, когда он сможет отправить новое сообщение
     * @return string|null
     */
    public static function checkExceeding(string $userIp, string $model, $timeRate = 3)
    {
        $timestamp = time() - 60 * $timeRate;

        if($message = $model::find()
            ->andWhere(['author_ip' => $userIp])
            ->andWhere(['>=', 'created_at', $timestamp])
            ->one()
        ) {
            return date('H:i:s', $message->created_at + 60 * $timeRate);
        }

        return null;
    }
}