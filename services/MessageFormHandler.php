<?php

namespace app\services;

use app\interfaces\RequestHandlerInterface;
use app\models\Message;
use yii\web\Request;

class MessageFormHandler implements RequestHandlerInterface
{
    /**
     * Создает сообщение и записывает в базу данных
     */
    public function run(Request $request): bool
    {
        $formData = $request->post('MessageForm');
        $authorName = trim($formData['author']);
        $text = trim($formData['text']);

        if($text && $authorName) {
            $authorIp = $request->userIP;

            $message = new Message();
            $message->author_name = $authorName;
            $message->author_ip = $authorIp;
            $message->text = $text;

            if($message->validate() && $message->save()) {
                return true;
            }
        }

        return false;
    }
}